'use strict';

/**
 * Dashs.js controller
 *
 * @description: A set of functions called "actions" for managing `Dashs`.
 */

module.exports = {

  /**
   * Retrieve dashs records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.dashs.search(ctx.query);
    } else {
      return strapi.services.dashs.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a dashs record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.dashs.fetch(ctx.params);
  },

  /**
   * Count dashs records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.dashs.count(ctx.query);
  },

  /**
   * Create a/an dashs record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.dashs.add(ctx.request.body);
  },

  /**
   * Update a/an dashs record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.dashs.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an dashs record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.dashs.remove(ctx.params);
  }
};
