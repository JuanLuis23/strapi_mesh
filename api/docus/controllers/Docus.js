'use strict';

/**
 * Docus.js controller
 *
 * @description: A set of functions called "actions" for managing `Docus`.
 */

module.exports = {

  /**
   * Retrieve docus records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.docus.search(ctx.query);
    } else {
      return strapi.services.docus.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a docus record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.docus.fetch(ctx.params);
  },

  /**
   * Count docus records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.docus.count(ctx.query);
  },

  /**
   * Create a/an docus record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.docus.add(ctx.request.body);
  },

  /**
   * Update a/an docus record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.docus.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an docus record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.docus.remove(ctx.params);
  }
};
